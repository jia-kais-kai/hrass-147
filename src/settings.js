module.exports = {
  // 设置浏览器title
  title: '147人力资源项目',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 固定布局的头部
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 设置侧边栏的logo图片
  sidebarLogo: true
}
