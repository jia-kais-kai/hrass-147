import Vue from 'vue'

// 重置css的代码
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// 引入elementUI组件 以及样式  语言包
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

// 全局的css引入
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
// 引入权限文件 会执行里边的代码
import '@/permission' // permission control

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

console.log(process.env)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
