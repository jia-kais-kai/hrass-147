import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally
// 注册全局组件  组件名svg-icon
// <svg-icon></svg-icon>
Vue.component('svg-icon', SvgIcon)

// require.context 使用https://zhuanlan.zhihu.com/p/59564277
// 第一个参数：查找文件路劲
// 第二个参数：是否查找子目录
// 第三个参数： 正则 表示需要查找哪些文件
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
