import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
// app 和 settings不要删 也不要改。这两个文件是帮咱们处理交互的
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user
  },
  getters
})

export default store
